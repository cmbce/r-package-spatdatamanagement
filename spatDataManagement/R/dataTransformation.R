#' @name kernels
#' @title Kernels linking distance to value
#' @description Compute values for classic dispersion kernels from a matrix of distances matdist
#' @param T multiplier for the values
#' @param matdist a matrix of distances, accepts the \code{spam} format, 2 to 10 times faster
#' @param f the characteristic distance, a scaling parameter, deviding the distance
#' @return expKernel:  
#' @references \url{http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1002801}
#' @seealso GridFromSample
#' @examples
#' # presentation of the kernels:
#' threshold <- 50
#' fprior <- 10
#' xabs<-seq(0,threshold)
#' plot(xabs,expKernel(1,xabs,fprior),type='l',lty=1,xlab='distance (m)',
#'      ylab='strength of the association')
#' lines(xabs,gaussianKernel(1,xabs,fprior),type='l',lty=2)
#' lines(xabs,cauchyKernel(1,xabs,fprior),type='l',lty=3)
#' lines(xabs,geometricKernel(1,xabs,fprior),type='l',lty=4)
#' legend('topright',c('Exponential','Gaussian','Cauchy','Geometric'),lty=c(1,2,3,4))
#' @rdname kernels
#' @export expKernel
expKernel <- function(T, matdist, f) {
    if (class(matdist) == "spam") {
        K <- matdist
        K@entries <- T * exp(-matdist@entries/f)
    } else {
        K <- T * exp(-matdist/f)
    }
    # K<- T*exp(-log(2)*matdist/f); ## normalized to have 0.5 when dist=f
    return(K)
}
#' @rdname kernels
#' @export gaussianKernel
gaussianKernel <- function(T, matdist, f) {
    if (class(matdist) == "spam") {
        K <- matdist
        K@entries <- T * exp(-(matdist@entries * matdist@entries)/f^2)
    } else {
        K <- T * exp(-(matdist * matdist)/f^2)
    }
    # K<- T*exp(-log(2)*(matdist*matdist)/f^2);## normalized to have 0.5 when dist=f
    return(K)
}
#' @rdname kernels
#' @export cauchyKernel
cauchyKernel <- function(T, matdist, f) {
    if (class(matdist) == "spam") {
        K <- matdist
        K@entries <- T/(1 + (matdist@entries * matdist@entries)/f^2)
    } else {
        K <- T/(1 + (matdist * matdist)/f^2)
    }
    return(K)
}
#' @rdname kernels
#' @export geometricKernel
geometricKernel <- function(T, matdist, f) {
    if (class(matdist) == "spam") {
        K <- matdist
        K@entries <- T/(1 + matdist@entries/f)
    } else {
        K <- T/(1 + matdist/f)
    }
    return(K)
}


#### Extrapolate points on a grid than can be ploted using persp() or for really nice things persp3d in
#### package rgl (see Transect_functions.R for examples)

# service function for GridFromSample
adjust.lim <- function(limsmall, limbig, steps = NULL, stepsize = NULL) {
    # limsmall: c(min,max) for coord with smallest range limbig: c(min,max) for coord with biggest range
    limsmall <- c(min(limsmall), max(limsmall))
    if (is.null(stepsize)) {
        steps.small <- steps
        stepsize <- (limsmall[2] - limsmall[1])/steps.small
    }
    
    limbig <- c(min(limbig), max(limbig))
    size.big.init <- (limbig[2] - limbig[1])
    steps.big <- ceiling(size.big.init/stepsize)
    shift.big <- (steps.big * stepsize - size.big.init)/2
    limbig <- c(limbig[1] - shift.big, limbig[2] + shift.big)
    
    return(list(limsmall = limsmall, limbig = limbig, stepsize = stepsize))
}
#' @name GridFromSample
#' @title Generate values on a grid from values on X,Y
#' @description Extrapolate values on a grid from values at given coordinates X and Y
#'     several distance kernels can be used for the extrapolation, see \code{\link{kernels}}
#'     the bandwith and many parameters can be set though a reasonable default is calculated 
#'     depending on the distribution of the X and Y
#' @param known.x the X locations of the measures
#' @param known.y the Y locations of the measures
#' @param known.z the value for the measures
#' @param kern the kernel to be used, a function such as defined in \code{\link{kernels}}
#' @param cumul If TRUE use cumulated values in spite of average
#' @param f bandwith or characteristic distance (see \code{?kernels})
#' @param T the kernel multiplier (see \code{?kernels})
#' @param xlim a vector with the min and max on X for the extrapolation
#' @param ylim same for Y
#' @param pixel.size size of the cells for the extrapolation, alternatively, the number of cells can be chosen (see parameter \code{steps})
#' @param steps number of cells on the smaller number
#' @param tr threshold distance at which the extrapolation is forced to 0 (the kernel is considered nul). 
#'      The higher this value, the slower the function...
#' @examples
#' # define measures not quite on a grid
#' angle <- seq(0,pi,pi/10)
#' xBase <- cos(angle)
#' yBase <- sin(angle)
#' x1 <- xBase*1
#' x2 <- xBase*2
#' x3 <- xBase*3
#' y1 <- yBase*1
#' y2 <- yBase*2
#' y3 <- yBase*3
#' z1 <- rep(1,length(x1))
#' z2 <- rep(2,length(x2))
#' z3 <- rep(3,length(x3))
#' x <- c(x1,x2,x3)
#' y <- c(y1,y2,y3)
#' z <- c(z1,z2,z3)
#' # now let's have a basic view of what's going on
#' par(mfrow=c(2,2))
#' gridMap <- GridFromSample(x,y,z)
#' with(gridMap,image(xs,ys,zs,asp=1,main='Default'))
#' points(x,y,asp=1,cex=z)
#' # let us tweak it a bit
#' gridMap1<- GridFromSample(x,y,z,tr = 0.5,ylim=c(0,5),xlim=c(-4,4))
#' with(gridMap1,image(xs,ys,zs,asp=1,main='tr=0.5 and reasonable limits'))
#' points(x,y,asp=1,cex=z)
#' gridMap2<- GridFromSample(x,y,z,tr = 1,ylim=c(0,5),xlim=c(-4,4))
#' with(gridMap2,image(xs,ys,zs,asp=1,main='tr=1 and reasonable limits'))
#' points(x,y,asp=1,cex=z)
#' gridMap3<- GridFromSample(x,y,z,tr = 3,ylim=c(0,5),xlim=c(-4,4))
#' with(gridMap3,image(xs,ys,zs,asp=1,main='tr=3 and reasonable limits'))
#' points(x,y,asp=1,cex=z)
#' @export GridFromSample
GridFromSample <- function(known.x, known.y, known.z, kern = expKernel, cumul = FALSE, f = NULL, T = 1, 
    xlim = NULL, ylim = NULL, pixel.size = NULL, steps = NULL, tr = NULL) {
    sizeSmallerSide <- min(max(known.x) - min(known.x), max(known.y) - min(known.y))
    if (is.null(tr)) {
        tr <- sizeSmallerSide/sqrt(length(known.x)) * 2
    }
    if (is.null(xlim)) {
        xlim = c(min(known.x) - tr, max(known.x) + tr)
    }
    if (is.null(ylim)) {
        ylim = c(min(known.y) - tr, max(known.y) + tr)
    }
    if (is.null(f)) {
        f <- tr/4
    }
    if (is.null(pixel.size)) {
        # on average one point per pixel
        pixel.size <- sizeSmallerSide/10
    }
    
    # get ToGuess locations
    if (abs(xlim[2] - xlim[1]) > abs(ylim[2] - ylim[1])) {
        if (is.null(steps)) 
            out <- adjust.lim(ylim, xlim, stepsize = pixel.size) else out <- adjust.lim(ylim, xlim, steps = steps)
        ylim <- out$limsmall
        xlim <- out$limbig
        stepsize <- out$stepsize
    } else {
        if (is.null(steps)) 
            out <- adjust.lim(xlim, ylim, stepsize = pixel.size) else out <- adjust.lim(xlim, ylim, steps = steps)
        xlim <- out$limsmall
        ylim <- out$limbig
        stepsize <- out$stepsize
    }
    cat("xlim:", xlim, "ylim", ylim, "stepsize:", stepsize, "\n")
    # vectors with the xs and ys of the grid
    xs <- seq(xlim[1], xlim[2], stepsize)
    ys <- seq(ylim[1], ylim[2], stepsize)
    
    # coordinates for all each point
    ToGuess.x <- rep(xs, length(ys))
    ToGuess.y <- as.vector(sapply(ys, rep, length(xs)))
    
    # get distance matrix
    matdist <- nearest.dist(x = cbind(ToGuess.x, ToGuess.y), y = cbind(known.x, known.y), method = "euclidian", 
        delta = tr, upper = NULL)
    weightsKnownInToGuessRaw <- kern(T, matdist, f)  # raw weights
    
    # get normalized by ToGuess weights
    sumR <- drop(weightsKnownInToGuessRaw %*% rep(1, dim(weightsKnownInToGuessRaw)[2]))
    # isolated: further than tr
    filter <- matdist
    filter@entries <- rep(1, length(filter@entries))
    nNeigh <- drop(filter %*% rep(1, dim(filter)[2]))
    isolated <- which(nNeigh == 0)
    if (cumul) {
        weightsKnownInToGuess <- weightsKnownInToGuessRaw
    } else {
        sumRsimple <- sumR
        sumRsimple[isolated] <- 1
        NormMat <- diag.spam(1/sumRsimple)
        weightsKnownInToGuess <- NormMat %*% weightsKnownInToGuessRaw
    }
    
    ToGuess.z <- weightsKnownInToGuess %*% known.z
    ToGuess.z[isolated] <- NA
    
    return(list(x = ToGuess.x, y = ToGuess.y, z = ToGuess.z, xs = xs, ys = ys, zs = t(matrix(ToGuess.z, 
        nrow = length(ys), byrow = TRUE))))
    # ,raw.weights=weightsKnownInToGuessRaw,dists=matdist
} 
