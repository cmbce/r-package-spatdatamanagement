### spatDataManagement ###
A bunch of helper functions when handling spatial data in R (sp class)

### Install it ###
#### Under linux or mac
Either:

```
#!R

devtools::install_git("https://bitbucket.org/cmbce/r-package-spatdatamanagement/",subdir="spatDataManagement")

```
Or clone with git then: 

1. Launch R in the spatDataManagement folder 

2. Make sure devtools is installed (install.packages("devtools")) 

3. Document and install with devtools: 

```
#!R
    library(devtools)
    document()
    install()

```

#### Under windows installing my be a bit difficult but the following should work: 
1. install R on a path without spaces (ex: C:\R)
2. install Rtools
3. make sure R and Rtools are in your path (system > advanced parameters > environment variables)
4. in r-package-spatDataManagement build the package in CMD (create a .tar.tgz
R CMD build spatDataManagement
5. still in the CMD, install it (XXX being the version number, for example 0.1)
R CMD INSTALL –no-multiarch spatDataManagement_XXX.tar.gz 

### How to participate ###
Pull requests are welcome :-)
