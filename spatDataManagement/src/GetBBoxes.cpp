// -*- mode: C++; c-indent-level: 4; c-basic-offset: 4; indent-tabs-mode: nil; -*-
/*
 * =====================================================================================
 *
 *       Filename:  GetBBoxes.cpp
 *
 *    Description:  get the bbox for each polygon in SpatialPolygons[DataFrame]
 *
 *        Version:  1.0
 *        Created:  09/08/2017 13:10:52
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Barbu (CB), corentin.barbu@gmail.com
 *        Company:  INRA
 *
 * =====================================================================================
 */
// we only include RcppArmadillo.h which pulls Rcpp.h in for us
#include "string"
#include "RcppArmadillo.h"
// via the depends attribute we tell Rcpp to create hooks for
// RcppArmadillo so that the build process will know what to do
//
// [[Rcpp::depends(RcppArmadillo)]]
// simple example of creating two matrices and
// returning the result of an operatioon on them
//
// via the exports attribute we tell Rcpp to make this function
// available from R
using namespace Rcpp;
//' Get bounding box for each polygon/polyline
//' @description Gets the bounding box (range of x and y) for each item of a SpatialLines/Polygons (DataFrame or not) object.
//'    It is equivalent to applying the \code{sp::bbox} function to all polygons with lapply but it is 
//'    simpler to use and much faster (even on a toy example of a few hundred polygons but designed for datasets with millions, then easily gets > 20x faster). 
//'    It is an important function to speed up other more complex comparisons between sp objects such as over().
//' @param x A SpatialPolygons[DataFrame] or SpatialLines[DataFrame]
//' @return A matrix of same number of columns than x and 4 columns : xmin, ymin, xmax, ymax
//' @export
//' @examples
//' data(phillyCensusTract)
//' ## simple use
//' system.time(bboxes <- GetBBoxes(phillyCensusTracts))
//' #=> 0.002 seconds
//' ## same using bbox
//' system.time(bboxesRef <- matrix(unlist(lapply(phillyCensusTracts@polygons,bbox)),ncol=4,byrow=TRUE))
//' #=> 0.021 seconds
// [[Rcpp::export]]
SEXP GetBBoxesAsList( SEXP x ){
    // determines object type and adapts the search of coordinates
    S4 obj(x) ;
    std::string nameList; 
    std::string nameSubList;
    if(Rf_inherits(x, "SpatialLines") || Rf_inherits(x, "SpatialLinesDataFrame")){
        nameList = "lines";
        nameSubList = "Lines";
    }else if(Rf_inherits(x, "SpatialPolygons") || Rf_inherits(x, "SpatialPolygonsDataFrame")){
        nameList = "polygons";
        nameSubList = "Polygons";
    }else{
        ::Rf_error("In GetBBoxes, class must be Spatial[Polygons|Lines][DataFrame]");
    }
    List a =  obj.slot(nameList);

    // count items
    int nPol = a.length();
    // NumericMatrix bboxes(nPol,4);
    List bboxes(nPol);

    // get the range
    for(int iPol = 0;iPol < nPol;iPol++){
        S4 pol = a(iPol);
        List b = pol.slot(nameSubList);

        double minX = std::numeric_limits<double>::infinity();
        double maxX = -std::numeric_limits<double>::infinity();
        double minY = std::numeric_limits<double>::infinity();
        double maxY = -std::numeric_limits<double>::infinity();

        for(int iSP = 0; iSP < b.length(); iSP++){
            S4 subPol = b(iSP);
            NumericMatrix coords = subPol.slot("coords");
            // X
            NumericVector rangeX = range(coords(_,0));
            if(rangeX(0)<minX) minX = rangeX(0);
            if(rangeX(1)>maxX) maxX = rangeX(1);
            // Y
            NumericVector rangeY = range(coords(_,1));
            if(rangeY(0)<minY) minY = rangeY(0);
            if(rangeY(1)>maxY) maxY = rangeY(1);
        }

        NumericVector bbox(4);
        bbox(0) = minX;
        bbox(1) = minY;
        bbox(2) = maxX;
        bbox(3) = maxY;
        bboxes(iPol) = bbox;
        // bboxes(iPol,0) = minX;
        // bboxes(iPol,1) = minY;
        // bboxes(iPol,2) = maxX;
        // bboxes(iPol,3) = maxY;
    }
    // Rcpp::DataFrame BBoxes = Rcpp::DataFrame::create(Rcpp::Named("minX")=bboxes(_,0),
    //         Rcpp::Named("minY")=bboxes(_,1),
    //         Rcpp::Named("maxX")=bboxes(_,2),
    //         Rcpp::Named("maxY")=bboxes(_,3));

    return bboxes;// BBoxes;
}

